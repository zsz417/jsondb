﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    /// <summary>
    /// 分页方式
    /// </summary>
    public enum PageMode
    {
        /// <summary>
        /// id Not In (select top size  id from table)
        /// </summary>
        TOP,
        /// <summary>
        /// id > ( select Max(id) from (select top pageIndex*size id from table) as t )
        /// </summary>
        MAX
    }

    /// <summary>
    /// SQL字符串处理
    /// </summary>
    public class SqlString
    {
        /// <summary>
        /// 获取sql中的表名
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static string GetSqlTable(string sql)
        {
            string table = null;
            if (sql.IndexOf(" order ") != -1 || sql.IndexOf(" where ") != -1)
            {
                table = sql.Substring(sql.LastIndexOf("from") + 4).Trim();
                table = table.Substring(0, table.IndexOf(" "));
            }
            else
            {
                table = sql.Substring(sql.IndexOf("from") + 4);
            }

            return table;
        }
        /// <summary>
        /// 分析SQL,返回 获取数据条数sql 及 当前页sql
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="index"></param>
        /// <param name="size"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        public static string AnalyticsSql(string sql, int index, int size, PageMode mode)
        {//select * from table
            string keyid = null, columns = null, table = null, orderby = null, wherestr = null, originalSql = null;

            sql = sql.ToLower().Trim().Replace("\r\n", " ");
            originalSql = sql;
            originalSql = originalSql.Replace(
                originalSql.Substring(originalSql.IndexOf(" select ") + 8
                                      , originalSql.IndexOf(" from ") - 8 - originalSql.IndexOf(" select "))
                , " count(*) ");
            if (sql.IndexOf(" * ") != -1)
            {
                if (sql.IndexOf("|") != -1)
                {
                    keyid = sql.Substring(sql.IndexOf("|") + 1, sql.IndexOf(" ", sql.IndexOf("|")) - sql.IndexOf("|") - 1);
                }
                else
                {
                    keyid = "id";
                }
                columns = "*";
            }
            else
            {
                keyid = sql.Substring(sql.IndexOf("select") + 6, sql.IndexOf(",") - sql.IndexOf("select") - 6);
                columns = sql.Substring(sql.IndexOf("select") + 6, sql.IndexOf(" from ") - 6 - sql.IndexOf("select"));
            }
            if (sql.IndexOf(" where ") != -1)
            {
                wherestr = " where ";
                if (sql.IndexOf(" order ") != -1)
                    wherestr += sql.Substring(sql.IndexOf(" where ") + 7, sql.IndexOf(" order ") - sql.IndexOf(" where ") - 7);
                else
                    wherestr += sql.Substring(sql.IndexOf(" where ") + 7);
            }
            table = GetSqlTable(sql);
            if (sql.IndexOf(" order ") != -1)
            {
                orderby = sql.Substring(sql.LastIndexOf("by") + 2);
            }
            else
            {
                orderby = keyid;
            }

            if (mode == PageMode.MAX)
            {
                sql = "select top " + size.ToString() + " " + columns + " from " + table + " where  "
                    + keyid + ">isnull((select max (" + keyid + ") from (select top "
                    + (index * size).ToString() + " " + keyid.ToString() + " from " + table + wherestr
                    + " order by " + orderby + ") as T),0) order by " + keyid;
            }
            else
            {
                sql = "select top " + size.ToString() + " " + columns + " from " + table + " where  "
                    + keyid + " not in (select top " + (index * size).ToString() + " " + keyid.ToString() + " from " + table + wherestr
                    + " order by " + orderby + " ) order by " + keyid;
            }
            return originalSql + ";" + sql;
        }
    }
}
