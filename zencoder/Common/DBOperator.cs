using System;
using System.Reflection;
using System.Configuration;

namespace Common.DataAccess
{
	public class DBOperator
	{
		public static IDBOperator GetInstance()
		{            
			IDBOperator db = null;

            //读取配置的程序集名称
            string assemblyName = "Common";          
            string typeName = ConfigurationSettings.AppSettings["typeName"];


            //加载程序集(加载数据访问组件Common)
            Assembly ab = Assembly.Load(assemblyName);
            //通过程序集对象,创建数据访问实例对象
            db = (IDBOperator)ab.CreateInstance(typeName);
			return db;
		}
	}
}
