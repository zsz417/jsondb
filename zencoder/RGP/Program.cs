﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.DataAccess;
using System.Data;
using ZencoderDotNet.Api;
using System.IO;


namespace RGP
{
    class Program
    {
        static void Main(string[] args)
        {
            Program p = new Program();
            p.checkCommand();

        }
        public void checkCommand(){
            Console.WriteLine("please write command(1 or 2)");
            string command=Console.ReadLine();
            switch (command)
            {
                case "exit":
                    return;
                case "2":
                    check();
                    break;
                case "1":
                    create();
                    break;
                default:
                    Console.WriteLine("it is not a command");
                    break;
            }
            checkCommand();
        }
        public void check()
        {
            Console.WriteLine("Starting to check");            

            Console.WriteLine("check ok");
        }
        public void create()
        {
            Console.WriteLine("Starting to create");
            runRemoveRedundancy();
            Console.WriteLine("create ok");
        }
        public void runRemoveRedundancy()
        {
            IDBOperator idb = DBOperator.GetInstance();
            string sqlAccess = @"SELECT * FROM  Presets where right(VideoFile,3)='flv' and runstate=0";
            DataTable dt = idb.ReturnDataTable(sqlAccess);
            StreamWriter rw = File.AppendText("online.sql");
            StreamWriter rw1 = File.AppendText("restoreonline.sql");
            StreamWriter log = File.AppendText("logonline.txt");
            foreach (DataRow dr in dt.Rows)
            {
                string url = dr["VideoFile"].ToString();
                string path = dr["VideoFile"].ToString().Replace("flv", "mp4");
                int jid =0;
                string sql1 = string.Format(@"select * from staging where VideoFile='{0}' and presetid={1}", url, dr["presetid"].ToString());
                if (idb.ExistData(sql1))
                {
                    string sql = @"update Presets set runstate=2,resulturl='" + path + "' where presetid='" + dr["presetid"].ToString() + "'";
                    idb.ExeCmd(sql);
                    string linelog = string.Format(@"[{2}] presetid:{0},path:{1},updated by staging", dr["presetid"].ToString(), dr["VideoFile"].ToString(), DateTime.Now.ToLongTimeString());
                    log.WriteLine(linelog);
                    Console.WriteLine(linelog);
                }
                else
                {
                    string json = jsonJob(url, path);
                    Job job = Job.create(json, "json");
                    jid = job.id;
                    string linelog = string.Format(@"[{3}] presetid:{0},path:{1},jobid:{2}", dr["presetid"].ToString(), dr["VideoFile"].ToString(), jid, DateTime.Now.ToLongTimeString());
                    log.WriteLine(linelog);
                    Console.WriteLine(linelog);
                    string sql = @"update Presets set runstate=1,resulturl='" + path + "' where presetid='" + dr["presetid"].ToString() + "'";
                    idb.ExeCmd(sql);
                }                                
                string s = string.Format(@"update RGP_Preset_Presets set VideoFile='{0}' where presetid={1}", path, dr["presetid"].ToString());
                rw.WriteLine(s);
                string s1 = string.Format(@"update RGP_Preset_Presets set VideoFile='{0}' where presetid={1}", dr["VideoFile"].ToString(), dr["presetid"].ToString());
                rw1.WriteLine(s1);
            }
            rw.Flush();
            rw.Close();
            rw1.Flush();
            rw1.Close();
            log.Flush();
            log.Close();
        }
        public void runWithDot()
        {
            IDBOperator idb = DBOperator.GetInstance();
            string sqlAccess = @"SELECT * FROM  staging where right(VideoFile,3)='flv' and runstate=0";
            DataTable dt = idb.ReturnDataTable(sqlAccess);
            StreamWriter rw = File.AppendText("staging.sql");
            StreamWriter rw1 = File.AppendText("restorestaging.sql");
            StreamWriter log = File.AppendText("logstaging.txt");
            foreach (DataRow dr in dt.Rows)
            {
                string url = dr["VideoFile"].ToString();
                string path = dr["VideoFile"].ToString().Replace("flv", "mp4");
                string json = jsonJob(url, path);
                Job job = Job.create(json, "json");
                int jid = job.id;

                string sql = @"update staging set runstate=1,resulturl='" + path + "' where presetid='" + dr["presetid"].ToString() + "'";
                idb.ExeCmd(sql);
                //StreamWriter rw1 = File.AppendText("staging.sql");
                string linelog = string.Format(@"[{3}] presetid:{0},path:{1},jobid:{2}", dr["presetid"].ToString(), dr["VideoFile"].ToString(), jid, DateTime.Now.ToLongTimeString());
                log.WriteLine(linelog);
                Console.WriteLine(linelog);
                string s = string.Format(@"update RGP_Preset_Presets set VideoFile='{0}' where presetid={1}", path, dr["presetid"].ToString());
                rw.WriteLine(s);
                string s1 = string.Format(@"update RGP_Preset_Presets set VideoFile='{0}' where presetid={1}", dr["VideoFile"].ToString(), dr["presetid"].ToString());
                rw1.WriteLine(s1);
            }
            rw.Flush();
            rw.Close();
            rw1.Flush();
            rw1.Close();
            log.Flush();
            log.Close();
        }
        public string jsonJob(string url,string outpath)
        {
            string json = string.Format(@"{{
              ""api_key"": ""xx"",
              ""input"": ""{0}"",
              ""output"": [
                {{
                  ""url"": ""{1}"",
                  ""format"": ""mp4"",
                  ""video_codec"": ""h264"",
                  ""audio_codec"": ""mp3"",
                  ""upscale"": true,
                  ""video_bitrate"": 750,
                  ""audio_bitrate"": 48,
                  ""audio_sample_rate"": 44100,
                  ""audio_channels"": 2,
                  ""public"": 1,
                  ""h264_profile"": ""high""
                }}
              ]
            }}", url, outpath);
            return json;
        }
        public void testwithDotSDK()
        {
            string json = jsonJob("http://s3.amazonaws.com/rgpVideo/65.flv", "http://s3.amazonaws.com/rgpVideo/65.mp4");
            Job job = Job.create(json, "json");
            int jid = job.id;
        }
        /*
        public void jobs()
        {
            IDBOperator idb = DBOperator.GetInstance();
            string sqlAccess = @"SELECT * FROM  staging where right(VideoFile,3)='flv' and runstate=0";
            DataTable dt = idb.ReturnDataTable(sqlAccess);

            foreach (DataRow dr in dt.Rows)
            {
                Zencoder.Zencoder Zen = new Zencoder.Zencoder("d30cbe66746a44889e061bcab1bbe716", new System.Uri("https://app.zencoder.com/api/v2"));
                Output[] outputs = new Output[]
                {
                    new Output()
                    {
                        Label = "iPhone",
                        Url = dr["VideoFile"].ToString().Replace("flv","mp4"),
                        Format=Zencoder.MediaFileFormat.MPFour,
                        VideoCodec=Zencoder.VideoCodec.H264,
                        AudioCodec=Zencoder.AudioCodec.Mp3,
                        Upscale=true,
                        AudioBitrate=48,
                        VideoBitrate=750,
                        AudioSampleRate=44100,
                        AudioChannels=2,
                        Public=true,
                        H264Profile=Zencoder.H264Profile.High     
                    }
                };
                CreateJobResponse response = Zen.CreateJob(dr["VideoFile"].ToString(), outputs, null, null, true, true);
                int result = response.Id;
            }
        }
        public void testWithZenSDK()
        {
            Zencoder.Zencoder Zen = new Zencoder.Zencoder("3e9eb82cf6ca4da53f9ea5180c78a98c", new System.Uri("https://app.zencoder.com/api/v2"));
            
             Output[] outputs = new Output[]
                 {
                     new Output()
                     {
                         Label = "iPhone",
                         Url = "http://s3.amazonaws.com/rgpVideoTest1/5min2.mp4",
                         Format=Zencoder.MediaFileFormat.MPFour,
                         VideoCodec=Zencoder.VideoCodec.H264,
                         AudioCodec=Zencoder.AudioCodec.Mp3,
                         Upscale=true,
                         AudioBitrate=48,
                         VideoBitrate=750,
                         AudioSampleRate=44100,
                         AudioChannels=2,
                         Public=true,
                         H264Profile=Zencoder.H264Profile.High
                        
                     }
                 };
             CreateJobResponse response = Zen.CreateJob("http://s3.amazonaws.com/rgpVideoTest1/5min1.flv", outputs, null, null, true, true);
             int result = response.Id;
        }*/
    }
}
